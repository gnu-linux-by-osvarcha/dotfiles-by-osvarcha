# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal


mod = "mod4"
# terminal = guess_terminal()
mod1 = "alt"
mod2 = "control"

terminal = "alacritty"

home = os.path.expanduser('~')

# Colors
colors = [
    ["#292d3e", "#292d3e"],  # 0 - panel background
    ["#434758", "#434758"],  # 1 - lighter background
    ["#ffffff", "#ffffff"],  # 2 - white - font color for group names
    ["#464cd6", "#464cd6"],  # 3 - blue darker
    ["#8d62a9", "#8d62a9"],  # 4 - purple normal - color for other tab and odd widgets
    ["#668bd7", "#668bd7"],  # 5 - blue
    ["#e1acff", "#e1acff"],  # 6 - pink
    ["#000000", "#000000"],  # 7 - black
    ["#AD343E", "#AD343E"],  # 8 - red
    ["#db5d4d", "#db5d4d"],  # 9 - orange - current tab color
    ["#DA8C10", "#DA8C10"],  # 10 - yellow/gold
    ["#F7DC6F", "#F7DC6F"],  # 11 - yellow bright
    ["#f1ffff", "#f1ffff"],  # 12 - almost white
    ["#4c566a", "#4c566a"],  # 13 - greyish
    ["#387741", "#387741"],  # 14 - green
]

keys = [
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "Tab", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout(), desc="Toggle between layouts"),
    # Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "q", lazy.window.kill(),desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    # Key([mod], "r", lazy.spawncmd(),
    #     desc="Spawn a command using a prompt widget"),
    Key([mod], "r", lazy.spawn("rofi -show drun"),
        desc="Spawn a command using a prompt widget"),

    # Custom Keys
    # Key([mod], "v", lazy.spawn('pavucontrol')),
    Key([mod, "shift"], "f", lazy.window.toggle_floating(),
        desc="Intercambiar flotante a mosaico, viceversa"),
    Key([mod], "f", lazy.window.toggle_fullscreen(),
        desc="Intercambiar modo fullscreen"),
    # Key(["mod1", "control"], "o", lazy.spawn(home + '/.config/qtile/picom-toggle.sh')),
    
    # INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl s +5%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl s 5%- ")),
    # INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("pamixer -t")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pamixer -d 5")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pamixer -i 5")),


]

####################################################
################### GROUPS #########################
####################################################

groups = []

# Para teclado QWERTY
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0",]

group_labels = ["", "", "", "", "", "", "", "", "", "",]

# group_layouts = [
#     "monadtall", "monadtall", "monadtall", "monadtall", "monadtall",
#     "monadtall", "monadtall", "monadtall", "treetab", "floating",]

group_layouts = [
    "max", "columns", "treetab", "columns", "columns", "columns",
    "columns", "columns", "columns", "columns",
]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

layouts = [
    # layout.Columns(border_focus_stack=['#d75f5f', '#8f3d3d'], border_width=4),
    layout.Columns(
        # border_focus_stack = '#ff00d8',
        border_focus = '#ff00d8',
        border_width = 3,
        margin= 7 ,
    ),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

# widget_defaults = dict(
#     font='sans',
#     fontsize=12,
#     padding=3,
# )
# extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(
                    foreground=colors[0],
                    background=colors[0],
                    padding=0,
                    scale=1,
                    custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                ),
                widget.GroupBox(
                    fontsize=12,
                    margin_y=2,
                    margin_x=0,
                    padding_y=5,
                    padding_x=3,
                    borderwidth=3,
                    active=colors[2],
                    inactive=colors[1],
                    rounded=False,
                    highlight_method='block',
                    urgent_alert_method='block',
                    this_current_screen_border=colors[4],
                    this_screen_border=colors[9],
                    other_current_screen_border=colors[0],
                    other_screen_border=colors[0],
                    foreground=colors[2],
                    background=colors[0],
                    disable_drag=True,
                    hide_unused=True,
                ),
                widget.Sep(
                linewidth=0,
                    padding=10,
                    foreground=colors[2],
                    background=colors[0],
                ),
                widget.TaskList(
                    theme_mode="preferred",
                    highlight_method = 'block',
                    icon_size=14,
                    max_title_width=100,
                    rounded=True,
                    margin=0,
                    padding=3,
                    fontsize=10,
                    border=colors[9],
                    foreground=colors[2],
                    borderwidth = 0,
                    background=colors[0],
                    urgent_border=colors[2],
                    txt_floating=' ',
                    txt_minimized='_ ',
                ),
                widget.Systray(
                    background=colors[1],
                    icon_size=15,
                    padding=5
                ),
                # Visor
                widget.BatteryIcon(
                    background=colors[14],
                ),
                widget.Battery(
                    background=colors[14],
                    charge_char="+",
                    discharge_char="-",
                    low_porcentage=0.35,
                    format="{char} {percent:2.0%} {hour:d}:{min:02d}'",
                ),
                widget.TextBox(
                    text='',
                    foreground=colors[8],
                    background=colors[14],
                    padding=-4,
                    fontsize=37,
                ),
                widget.CPU(
                    foreground=colors[2],
                    background=colors[8],
                    format=" {freq_current}Ghz {load_percent}%",
                    mouse_callbacks={'Button1': lazy.spawn(terminal + " --hold -e watch -n 2 sensors")},
                ),

                # widget.ThermalSensor(
                #     foreground=colors[2],
                #     background=colors[8],
                #     format=" {temp:.1f} {°C}%",
                #     tag_sensor="temp1",
                # ),
                # widget.ThermalZone(
                #     zone="/sys/class/thermal/thermal_zone0/temp"
                # ),
                widget.TextBox(
                    text='',
                    foreground=colors[13],
                    background=colors[8],
                    padding=-4,
                    fontsize=37,
                ),
                widget.Memory(
                    foreground=colors[2],
                    background=colors[13],
                    format=" {MemUsed: .0f}{mm}",
                    # mouse_callbacks={
                    #     'Button1': lazy.spawn(terminal + " --hold -e bpytop"),                  # left click
                    #     'Button3': lazy.spawn(terminal + " --hold -e watch -n 2 free -th"),     # right click
                    # },
                ),
                widget.Wlan(
                    foreground=colors[2],
                    background=colors[4],
                    interface='wlo1',
                    # format=" {essid} {quality}/70",
                    format=" {quality}/70",
                    disconnected_message=''
                ),
                widget.WidgetBox(
                    background=colors[4],
                    text_open="",
                    text_closed="",
                    widgets=[
                        widget.Net(
                            foreground=colors[2],
                            background=colors[4],
                            prefix='M',
                            format="{down} {up}",
                            # mouse_callbacks={'Button1': lazy.spawn(terminal + " --hold -e ip addr")},
                        ),
                    ]),
                widget.Clock(
                    foreground=colors[2],
                    background=colors[5],
                    padding=4,
                    format=" %b %d %H:%M",
                    mouse_callbacks={'Button1': lazy.spawn("gsimplecal")},
                ),
                widget.OpenWeather(
                    location='Tacna,Pe',
                    format=' {location_city}: {main_temp} °{units_temperature} {humidity}% {weather_details}',
                    language='es',
                ),

                widget.QuickExit(
                    default_text='  off ',
                    countdown_format='[{}]',
                    background='#fe6c00',
                ),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# StartUp
@hook.subscribe.startup_once
def autostart():
    """Ejecuta Programas al iniciar"""
    home = os.path.expanduser('~')
    # subprocess.call([path.join(qtile_path, 'autostart.sh')])
    subprocess.call([home + '/.config/qtile/autostart.sh'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
