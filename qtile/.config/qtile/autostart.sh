#!/bin/sh

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}


# starting utility applications
lxsession &
run volumeicon &
blueman-applet &
flameshot &
feh --bg-fill ~/Pictures/wallpapers/Black\ Dragon.png &
picom --config ~/.config/picom/picom-blur.conf &
