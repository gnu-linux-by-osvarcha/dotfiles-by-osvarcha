set VIRTUAL_ENV_DISABLE_PROMPT "1"

# export PATH="/home/osvarcha/.guix-profile/bin:/home/osvarcha/.guix-profile/sbin"
# export TERMINFO_DIRS="/home/osvarcha/.guix-profile/share/terminfo"
# export GST_PLUGIN_SYSTEM_PATH="/home/osvarcha/.guix-profile/lib/gstreamer-1.0"
# export XDG_DATA_DIRS="/home/osvarcha/.guix-profile/share"
# export GIO_EXTRA_MODULES="/home/osvarcha/.guix-profile/lib/gio/modules"
# export GIT_EXEC_PATH="/home/osvarcha/.guix-profile/libexec/git-core"
# export EMACSLOADPATH="/home/osvarcha/.guix-profile/share/emacs/site-lisp"
# export INFOPATH="/home/osvarcha/.guix-profile/share/info"

if status is-interactive
    # Commands to run in interactive sessions can go here
end

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias wget='wget -c '

alias la='exa -alg --color=always --group-directories-first --icons' # preferred listing
alias ls='exa -a --color=always --group-directories-first --icons'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'  # long format
alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing
alias l.='exa -ald --color=always --group-directories-first --icons .*' # show only dotfiles

alias cat='bat --style header --style snip --style changes --style header'

alias grep='grep --color=auto'
alias fgrep='grep -F --color=auto'
alias egrep='grep -E --color=auto'